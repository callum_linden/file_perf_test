#include <iostream>
#include <fstream>
#include <iomanip>
#include <vector>
#include <string>
#include <random>
#include <algorithm>
#include <numeric>
#include <chrono>
#include <filesystem>

std::vector<uint64_t> generate_data(std::size_t bytes)
{
    std::vector<uint64_t> data(bytes / sizeof(uint64_t));
    std::iota(data.begin(), data.end(), 0);
    std::shuffle(data.begin(), data.end(), std::mt19937{ std::random_device{}() });
    return data;
}

const std::vector<std::string> build_pathnames(size_t file_count, bool use_sub_dirs)
{
    std::random_device rd;
    std::mt19937 mt(rd());
    std::uniform_int_distribution<int> dist('0', '9');

    std::vector<std::string> pathnames;

    std::filesystem::create_directory("./files");

    if (use_sub_dirs)
    {
        std::filesystem::create_directory("./files/0");
        std::filesystem::create_directory("./files/1");
        std::filesystem::create_directory("./files/2");
        std::filesystem::create_directory("./files/3");
        std::filesystem::create_directory("./files/4");
        std::filesystem::create_directory("./files/5");
        std::filesystem::create_directory("./files/6");
        std::filesystem::create_directory("./files/7");
        std::filesystem::create_directory("./files/8");
        std::filesystem::create_directory("./files/9");
    }

    const size_t random_char_length = 32;

    for (size_t i = 0; i < file_count; ++i)
    {
        std::string filename;
        for (size_t c = 0; c < random_char_length; ++c)
        {
            filename += dist(mt);
        }

        std::string pathname = "./files/";
        if (use_sub_dirs)
        {
            pathname += filename[0];
            pathname += "/";
        }
        pathname += filename;
        pathname += ".dat";

        //std::cout << "path name: " << pathname << std::endl;
        pathnames.push_back(pathname);
    }

    return pathnames;
}

long long write_files(std::vector<std::string> pathnames, size_t file_size, bool use_streams, bool use_sub_dirs)
{
    std::vector<uint64_t> file_data = generate_data(file_size);

    auto startTime = std::chrono::high_resolution_clock::now();

    if (use_streams)
    {
        std::vector<std::string>::iterator file_iter = pathnames.begin();
        while (file_iter != pathnames.end())
        {
            auto myfile = std::fstream(*file_iter, std::ios::out | std::ios::binary);
            myfile.write((char*)&file_data[0], file_size);
            myfile.close();

            ++file_iter;
        }
    }
    else
    {
        std::vector<std::string>::iterator file_iter = pathnames.begin();
        while (file_iter != pathnames.end())
        {
            FILE* file = fopen((*file_iter).c_str(), "wb");
            fwrite(&file_data[0], 1, file_size, file);
            fclose(file);

            ++file_iter;
        }
    }
    auto endTime = std::chrono::high_resolution_clock::now();

    return std::chrono::duration_cast<std::chrono::milliseconds>(endTime - startTime).count();
}

long long read_files(std::vector<std::string> pathnames, size_t file_size, bool use_streams, bool use_sub_dirs)
{
    auto startTime = std::chrono::high_resolution_clock::now();

    if (use_streams)
    {
        std::vector<std::string>::iterator file_iter = pathnames.begin();
        while (file_iter != pathnames.end())
        {
            std::ifstream input(*file_iter, std::ios::binary);
            std::vector<unsigned char> buffer(std::istreambuf_iterator<char>(input), {});

            if (buffer.size() != file_size)
            {
                std::cout << "STREAM read error: unable to read file: " << (*file_iter) << std::endl;
            }

            ++file_iter;
        }
    }
    else
    {
        std::vector<std::string>::iterator file_iter = pathnames.begin();
        while (file_iter != pathnames.end())
        {
            FILE* file_handle = fopen((*file_iter).c_str(), "rb");
            if (file_handle == NULL)
            {
                std::cout << "FILE* read error: unable to read file: " << (*file_iter) << std::endl;
            }

            fseek(file_handle, 0, SEEK_END);
            long eof_offset = ftell(file_handle);
            rewind(file_handle);

            char* buffer = (char*)malloc(sizeof(char) * eof_offset);
            if (buffer == NULL)
            {
                std::cout << "FILE* read error: unable to allocate memory: " << (*file_iter) << std::endl;
            }

            size_t result = fread(buffer, 1, eof_offset, file_handle);
            if (result != eof_offset)
            {
                std::cout << "FILE* read error: unable to allocate memory: " << (*file_iter) << std::endl;
            }

            fclose(file_handle);
            free(buffer);

            ++file_iter;
        }
    }
    auto endTime = std::chrono::high_resolution_clock::now();

    return std::chrono::duration_cast<std::chrono::milliseconds>(endTime - startTime).count();
}

long long run_test(bool writing, std::vector<std::string> pathnames, size_t file_size, bool use_streams, bool use_sub_dirs)
{
    if (writing)
    {
        return write_files(pathnames, file_size, use_streams, use_sub_dirs);
    }
    else
    {
        return read_files(pathnames, file_size, use_streams, use_sub_dirs);
    }
}

void display_result(bool writing, size_t file_count, size_t file_size, bool use_streams, bool use_sub_dirs, long long elapsed_time)
{
    std::cout << (writing ? "Writing " : "Reading ");
    std::cout << file_count << " files ";
    std::cout << "of size " << file_size << " ";
    std::cout << "using " << (use_streams ? "streams" : "FILE*") << " ";
    std::cout << "and using " << (use_sub_dirs ? "subdirectories" : "no subdirectories") << " ";
    std::cout << "took " << elapsed_time << "ms ";
	double ms_per_file = (double)elapsed_time / (double)file_count;
	std::cout << std::setprecision(2) << std::fixed;
	std::cout << "(" << ms_per_file << " ms per file)";
    std::cout << std::endl;
}

void run_suite(size_t file_size, size_t file_count)
{
    long long elapsed_time;
    bool writing;
    bool use_streams;
    bool use_sub_dirs;

    std::cout << std::endl << "Writing and reading with " << file_count << " files of size " << file_size << std::endl;

    // writing
    writing = true;
    use_streams = true;
    use_sub_dirs = true;
    std::vector<std::string> pathnames_tt = build_pathnames(file_count, use_sub_dirs);
    elapsed_time = run_test(writing, pathnames_tt, file_size, use_streams, use_sub_dirs);
    display_result(writing, file_count, file_size, use_streams, use_sub_dirs, elapsed_time);

    writing = true;
    use_streams = true;
    use_sub_dirs = false;
    std::vector<std::string> pathnames_tf = build_pathnames(file_count, use_sub_dirs);
    elapsed_time = run_test(writing, pathnames_tf, file_size, use_streams, use_sub_dirs);
    display_result(writing, file_count, file_size, use_streams, use_sub_dirs, elapsed_time);

    writing = true;
    use_streams = false;
    use_sub_dirs = true;
    std::vector<std::string> pathnames_ft = build_pathnames(file_count, use_sub_dirs);
    elapsed_time = run_test(writing, pathnames_ft, file_size, use_streams, use_sub_dirs);
    display_result(writing, file_count, file_size, use_streams, use_sub_dirs, elapsed_time);

    writing = true;
    use_streams = false;
    use_sub_dirs = false;
    std::vector<std::string> pathnames_ff = build_pathnames(file_count, use_sub_dirs);
    elapsed_time = run_test(writing, pathnames_ff, file_size, use_streams, use_sub_dirs);
    display_result(writing, file_count, file_size, use_streams, use_sub_dirs, elapsed_time);

    // reading
    writing = false;
    use_streams = true;
    use_sub_dirs = true;
    elapsed_time = run_test(writing, pathnames_tt, file_size, use_streams, use_sub_dirs);
    display_result(writing, file_count, file_size, use_streams, use_sub_dirs, elapsed_time);

    writing = false;
    use_streams = true;
    use_sub_dirs = false;
    elapsed_time = run_test(writing, pathnames_tf, file_size, use_streams, use_sub_dirs);
    display_result(writing, file_count, file_size, use_streams, use_sub_dirs, elapsed_time);

    writing = false;
    use_streams = false;
    use_sub_dirs = true;
    elapsed_time = run_test(writing, pathnames_ft, file_size, use_streams, use_sub_dirs);
    display_result(writing, file_count, file_size, use_streams, use_sub_dirs, elapsed_time);

    writing = false;
    use_streams = false;
    use_sub_dirs = false;
    elapsed_time = run_test(writing, pathnames_ff, file_size, use_streams, use_sub_dirs);
    display_result(writing, file_count, file_size, use_streams, use_sub_dirs, elapsed_time);
}

int main()
{
    size_t file_size;
    size_t file_count;

    file_size = 1024 * 1024;
    file_count = 10;
    run_suite(file_size, file_count);

	file_size = 1024 * 1024;
	file_count = 100;
	run_suite(file_size, file_count);

	file_size = 1024 * 1024;
	file_count = 1000;
	run_suite(file_size, file_count);

	file_size = 1024 * 1024;
	file_count = 5000;
	run_suite(file_size, file_count);
}
