## Results ##

##### Some preliminary results from my system #####
(MacBook Pro 2019, Win 10/64 (BootCamp), 32GB RAM, SSD)

    Writing and reading with 10 files of size 1048576
    Writing 10 files of size 1048576 using streams and using subdirectories took 44ms (4.40 ms per file)
    Writing 10 files of size 1048576 using streams and using no subdirectories took 37ms (3.70 ms per file)
    Writing 10 files of size 1048576 using FILE* and using subdirectories took 38ms (3.80 ms per file)
    Writing 10 files of size 1048576 using FILE* and using no subdirectories took 34ms (3.40 ms per file)
    
    Reading 10 files of size 1048576 using streams and using subdirectories took 120ms (12.00 ms per file)
    Reading 10 files of size 1048576 using streams and using no subdirectories took 98ms (9.80 ms per file)
    Reading 10 files of size 1048576 using FILE* and using subdirectories took 3ms (0.30 ms per file)
    Reading 10 files of size 1048576 using FILE* and using no subdirectories took 4ms (0.40 ms per file)

    Writing and reading with 100 files of size 1048576
    Writing 100 files of size 1048576 using streams and using subdirectories took 319ms (3.19 ms per file)
    Writing 100 files of size 1048576 using streams and using no subdirectories took 318ms (3.18 ms per file)
    Writing 100 files of size 1048576 using FILE* and using subdirectories took 315ms (3.15 ms per file)
    Writing 100 files of size 1048576 using FILE* and using no subdirectories took 312ms (3.12 ms per file)

    Reading 100 files of size 1048576 using streams and using subdirectories took 965ms (9.65 ms per file)
    Reading 100 files of size 1048576 using streams and using no subdirectories took 976ms (9.76 ms per file)
    Reading 100 files of size 1048576 using FILE* and using subdirectories took 37ms (0.37 ms per file)
    Reading 100 files of size 1048576 using FILE* and using no subdirectories took 37ms (0.37 ms per file)

    Writing and reading with 1000 files of size 1048576
    Writing 1000 files of size 1048576 using streams and using subdirectories took 3234ms (3.23 ms per file)
    Writing 1000 files of size 1048576 using streams and using no subdirectories took 3154ms (3.15 ms per file)
    Writing 1000 files of size 1048576 using FILE* and using subdirectories took 3158ms (3.16 ms per file)
    Writing 1000 files of size 1048576 using FILE* and using no subdirectories took 3218ms (3.22 ms per file)

    Reading 1000 files of size 1048576 using streams and using subdirectories took 9126ms (9.13 ms per file)
    Reading 1000 files of size 1048576 using streams and using no subdirectories took 9019ms (9.02 ms per file)
    Reading 1000 files of size 1048576 using FILE* and using subdirectories took 378ms (0.38 ms per file)
    Reading 1000 files of size 1048576 using FILE* and using no subdirectories took 410ms (0.41 ms per file)

    Writing and reading with 5000 files of size 1048576
    Writing 5000 files of size 1048576 using streams and using subdirectories took 16577ms (3.32 ms per file)
    Writing 5000 files of size 1048576 using streams and using no subdirectories took 16128ms (3.23 ms per file)
    Writing 5000 files of size 1048576 using FILE* and using subdirectories took 15495ms (3.10 ms per file)
    Writing 5000 files of size 1048576 using FILE* and using no subdirectories took 16399ms (3.28 ms per file)

    Reading 5000 files of size 1048576 using streams and using subdirectories took 45972ms (9.19 ms per file)
    Reading 5000 files of size 1048576 using streams and using no subdirectories took 44729ms (8.95 ms per file)
    Reading 5000 files of size 1048576 using FILE* and using subdirectories took 1840ms (0.37 ms per file)
    Reading 5000 files of size 1048576 using FILE* and using no subdirectories took 1860ms (0.37 ms per file)


At a very superficial level, we can see that:

* The number of files in play appears to have no effect and the read/write time per file is consistent. 
* Writing/reading using a collection of subdirectories vs a single directory seems to make no difference
* Reading using FILE* vs streams appears to be ~ 30X faster

    * That's such a big and unexpected difference that it warrants a deeper dive.
    * OS level caching perhaps, but should be the same for streams too?
    * Logic error in the code? Definitely possible, even likely
