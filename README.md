## Testing file write/read perfornamce ###

#### Variations:
* Reading and writing files
* Small, medium, large numbers of files
* Using C FILE* vs C++ streams
* Storing files in a single directory vs many subdirectories

#### Building
##### Windows 64

    mkdir build
    cd build
    cmake -G "Visual Studio 15 2017 Win64" ..
    start file_perf_test.sln

